#### This exercise aims at creating a topic, decribe a topic and delete a topic

#### Start the Kafka cluster
In the CONSOLE directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  
The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry 
The docker-compose file contains also a base container that allows to run Kafka commands

#### Interact with Kafka cluster
Get into the base container    
```docker exec -it base bash```
		
#### Create a topic
List al available options for the kafka-topics command  
```kafka-topics```  
Create a topic named consoleTopic using the kafka-topics command, replicated 3 times and with 3 partitions  
```kafka-topics --create --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092 --topic consoleTopic --replication-factor 3 --partitions 3 --config cleanup.policy=delete --config segment.bytes=1073741824```

#### List existing topics
List the existing topics using the --list option of the kafka-topics command  
```kafka-topics --list --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092```  

#### Describe a topic
Describe the topic consoleTopic using the --describe option of the kafka-topics command  
```kafka-topics --describe --topic consoleTopic --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092``` 

#### Alter a topic
Alter the topic consoleTopic using the kafka-configs command  
Add the retention.ms option to 604800000  
Delete the segment.bytes option  
```kafka-configs --zookeeper zookeeper:2181 --alter --entity-type topics --entity-name consoleTopic --add-config retention.ms=604800000```  
```kafka-configs --zookeeper zookeeper:2181 --entity-type topics --alter --entity-name consoleTopic --delete-config segment.bytes``` 
#### Describe a topic
Describe the topic consoleTopic using the --describe option of the kafka-topics command  
```kafka-topics --describe --topic consoleTopic --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092``` 

#### Delete a topic
Delete the topic consoleTopic using the --delete option of the kafka-topics command  
```kafka-topics --delete --topic consoleTopic --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092``` 

#### List existing topics
Check that simple topic has been deleted using the --list option of the kafka-topics command  
```kafka-topics --list --bootstrap-server kafka-1:9092,kafka-2:9092,kafka-3:9092```  

#### Stop and remove containers 
 ```docker-compose stop```   
 ```docker-compose rm -f```  