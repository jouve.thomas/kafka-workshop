package partial.com.kafka.workshop.producer;

import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class SimpleProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = TopicsNames.SIMPLE_TOPIC;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Topics.createTopics(topic);

        Properties props = new Properties();
        // TODO: Set the bootstrap servers

        // TODO: Set acks config to all

        // TODO: Set retries config to 3

        // TODO: Set the key and value serializers

        //TODO: set the batch size config to default value

        // TODO: set the linger ms config to default value 0


        // TODO: Create an Producer instance with String key and Value key
        Producer<String, String> producer = null;


        String key = "";
        String value = "";
        try {
            //sending 100 messages
            for (int i = 0; i < 100; i++) {
                key = getAlphaNumericString(5);
                value = getAlphaNumericString(20);
                // TODO: Create a producer record instance with String key and Value key
                ProducerRecord<String, String> record = null;

                // TODO: Add some headers (for ex: add the key as header)


                boolean getCallBack = true;
                if (!getCallBack) {
                    // TODO: Send record

                } else {
                    // TODO: Send record to Kafka using callBack

                }

            }
        } finally {
            producer.close();
        }
    }

    private void buildHeader(ProducerRecord<String, String> record, String headerKey, String headerValue) {
        // TODO: Create an instance of a record header and add the header to the record
        RecordHeader recordHeader = null;
    }
    private String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


}
