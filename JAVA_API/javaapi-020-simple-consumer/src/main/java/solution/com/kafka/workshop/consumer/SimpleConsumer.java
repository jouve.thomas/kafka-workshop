package solution.com.kafka.workshop.consumer;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@SpringBootApplication
@Component
public class SimpleConsumer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    // TODO: Set the topic name
    private String topic = TopicsNames.SIMPLE_TOPIC;

    private boolean seekTobeginning = true;

    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic);

        Properties props = new Properties();
        // TODO: Set the bootstrap servers
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // TODO: Set the group id, use the TopicsNames.PREFIX
        props.put(ConsumerConfig.GROUP_ID_CONFIG, TopicsNames.PREFIX + "groupid");

        // TODO: Set the key and value deserializers
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        // TODO: Set the auto reset offset to the earliest commited offset
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // TODO: Set the auto commit config to false
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        
   //     props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 10);

        // TODO: Create an instance of a Consumer with String key and Value key
        Consumer<String, String> consumer = new KafkaConsumer<>(props);

        // TODO: Subscribe to topic
        consumer.subscribe(Arrays.asList(topic));

        // TODO: Seek to beginning. While partition assignment is not done, poll(0)
        if(seekTobeginning) {
            while (consumer.assignment().size() == 0) {
                consumer.poll(Duration.ofMillis(0));
            }
            consumer.seekToBeginning(consumer.assignment());
        }

        try {
            while (true) {
                // TODO: Poll records and iterate on each records
                // TODO: Display the record key, the record value, the record partition and the record offset
                // TODO: Display the record header. Tips: get the last header value
                ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));
                logger.info("{} have been polled", consumerRecords.count());
                consumerRecords.forEach(record -> {
                    if(record.headers() == null || record.headers().lastHeader("record_key") == null){
                        logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset());
                    }else{
                        logger.info("consuming message key={} value={} partition={} offset={} key header={}" , record.key(),  record.value(), record.partition(), record.offset(), new String(record.headers().lastHeader("record_key").value()));
                    }

                });

                // TODO Commit last offset polled if there are some consumerRecords 
                if(consumerRecords.count() > 1) {
                    consumer.commitSync();
                }
            }
        }finally {
            consumer.close();
        }

    }



}
