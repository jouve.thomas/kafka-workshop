#### This exercise aims at writing a java KAFKA producer using SASL authentication

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose file
```docker-compose -f docker-compose-sasl.yml up -d```  
The cluster is composed of 1 zookeeper and 3 brokers  
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records  
The cluster is configured to use SASL_PLAINTEXT protocol with the SCRAM-SHA-512 SASL mechanism  
```advertised.listeners=SASL_PLAINTEXT://localhost:19092```  
```listeners=SASL_PLAINTEXT://0.0.0.0:29092  ```
```security.inter.broker.protocol=SASL_PLAINTEXT```    
```sasl.enabled.mechanisms=SCRAM-SHA-512```    
```sasl.mechanism.inter.broker.protocol=SCRAM-SHA-512```  
Each broker is started with KAFKA_OPTS options: ```-Djava.security.auth.login.config=/etc/kafka/secrets/broker_jaas.conf```  
Each zookeeper is started with KAFKA_OPTS options: ```-Djava.security.auth.login.config=/etc/kafka/secrets/zookeeper_jaas.conf```  

##### In the partial.com.kafka.workshop.producer.SaslProducer java class:
#### Topic name
Set the topic name you will produce records to using "topic" variable. Set it to saslTopic
#### Produce records
Start the application  
Note the error ```org.apache.kafka.clients.NetworkClient   : [Producer clientId=producer-1] Bootstrap broker localhost:39092 (id: -3 rack: null) disconnected```  
The producer must provide authentication to the cluster in order to produce records. 

#### Configure the SASL authentication
The SCRAM implementation in Kafka stores SCRAM credentials in ZooKeeper and is suitable for use in Kafka installations where ZooKeeper is on a private network.  
Because of this, you must create SCRAM credentials for users in ZooKeeper.  
Get into the base container  
```docker exec -it base bash```  
Using the kafka-configs command, create the admin user withe admin-secret as password  
```kafka-configs  --zookeeper zookeeper:2181 --alter --add-config 'SCRAM-SHA-512=[password=admin-secret]' --entity-type users --entity-name admin```  
Exit the container  
```exit;```  
Configure the KAFKA producer using the org.apache.kafka.common.config.SaslConfigs and org.apache.kafka.clients.CommonClientConfigs classes  
- the security protocol configuration using the CommonClientConfigs.SECURITY_PROTOCOL_CONFIG constant. Set it to SASL_PLAINTEXT  
- the SASL mechanism configuration using the SaslConfigs.SASL_MECHANISM constant. Set it to SCRAM-SHA-512  
- the SASL JAAS configuration using the SaslConfigs.SASL_JAAS_CONFIG constants. It must contains credentials created previously:  
```"org.apache.kafka.common.security.scram.ScramLoginModule required username=\"admin\" password=\"admin-secret\";");```  		
#### Produce records :
Start the application 	
Records are produced successfully  
#### Add another users
Get into the base container  
```docker exec -it base bash```  
Using the kafka-configs command, create the myUser user withe admin-secret as password  
```kafka-configs  --zookeeper zookeeper:2181 --alter --add-config 'SCRAM-SHA-512=[password=myUserPassword]' --entity-type users --entity-name myUser```  
Exit the container  
```exit;```  
Change the SASL JAAS configuration in order to authenticate the cluster with myUser  
Start the application  
Records are produced successfully  
#### Add ACLs
Change the SASL JAAS configuration in order to authenticate the cluster with myUser and start the producer  
Note the error ```"Not authorized to access topics: [saslTopic]"```  
ACLs must be added to the myUser to WRITE data to saslTopic  
Get into the base container  
```docker exec -it base bash```  
Using the kafka-acl command, add the WRITE to saslTopic authorization to the myUser user  
```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --add --allow-principal User:myUser --operation WRITE --topic saslTopic```   
List the existing ACLs           
```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --list```  
Exit the container  
```exit;```  
Start the producer  
Records are successfully sent to the cluster  

#### Consume data from Java application
Go to the next exercice: 06_sasl_consumer  
Do not stop your containers !  
#### Consume data from Kafka console consumer
Go to the next exercice: 05_sasl_consumer  
Do not stop your containers !  