#### This exercise aims at writing a java KAFKA producer producing AVRO records

#### Start the Kafka cluster :
If your Docker containers of the previous exercise 08_schema_registry are stopped,   
```docker-compose start```  
The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records  

##### In the partial.com.kafka.workshop.producer.AvroProducer java class:
#### Topic name:
Set the topic name you will produce records to using "topic" variable. Set it to avroTopic  
#### Generate Avro classes
Compile the project, and check that Avro classes are generated from the avsc files in \src\main\resources\avro\  
Avro classes are generated with the avro-maven-plugin configured in the pom.xml file  
#### Schema Registry configuration
Configure the Schema Registry using the io.confluent.kafka.serializers.KafkaAvroSerializerConfig class  
- the schema registry url using the SCHEMA_REGISTRY_URL_CONFIG constant  
#### Producer configuration
Configure the KAFKA producer using the org.apache.kafka.clients.producer.ProducerConfig class  
- the serializer for the key and the value of the record to produce using the KEY_SERIALIZER_CLASS_CONFIG and VALUE_SERIALIZER_CLASS_CONFIG constants  
In this exercise Avro records will be produced: KafkaAvroSerializer.class
#### Produce records
- Create a org.apache.kafka.clients.producer.KafkaProducer instance with WorkshopKey and WorkshopValue as generic types
- Inside the For Loop, create a org.apache.kafka.clients.producer.ProducerRecord instance, pass the topic name, the key and value as arguments
- Send each record to Kafka topic using the ```producer.send(Record, new Callback() {...} )``` method
#### Update the Avro schema
- replace the WorkshopValue.avsc file with the WorkshopValue.avsc_v2 file
- compile the project and fix the compilation errors
- send some new records   
#### Consume records from Java application
Go to the next exercice: 10_avro_consumer
Do not stop your containers !
#### Consume records from Kafka console consumer
Go to the next exercice: 11_avro_console_consumer
Do not stop your containers !
