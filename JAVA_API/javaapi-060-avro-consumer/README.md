#### This exercise aims at writing a java KAFKA consumer consuming AVRO records

#### Start the Kafka cluster :
If your Docker containers of the previous exercise 08_schema_registry are stopped,   
```docker-compose start```  
The cluster is composed of 1 zookeeper, 3 broker and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records  
	
##### In the partial.com.kafka.workshop.consumer.AvroConsumer java class:
#### Topic name:
Set the topic name you will consume records from using "topic" variable. Set it to avroTopic 
#### Generate Avro classes
Compile the project, and check that Avro classes are generated from the avsc files in \src\main\resources\avro\  
Avro classes are generated with the avro-maven-plugin configured in the pom.xml file  
#### Schema Registry configuration :
Configure the Schema Registry using the io.confluent.kafka.serializers.KafkaAvroSerializerConfig class  
- the schema registry url using the SCHEMA_REGISTRY_URL_CONFIG constant  
#### Consumer configuration :
Configure the KAFKA consumer using the org.apache.kafka.clients.consumer.ConsumerConfig class  
- the deserializer for the key and the value of the record to consume using the KEY_DESERIALIZER_CLASS_CONFIG and VALUE_DESERIALIZER_CLASS_CONFIG constants  
in this execrice we will consume some Avro messages: KafkaAvroDeserializer.class  
#### Consuming records :
- Create a org.apache.kafka.clients.consumer.KafkaConsumer instance with WorkshopKey and WorkshopValue as generic types  
- Subscribe to the topic using the subscribe method of the KafkaConsumer instance  
- In the while loop :  
create a ConsumerRecords using the poll method of the KafkaConsumer instance  
iterate throught the polled list of records using the forEach method of the ConsumerRecords instance  
display the key, value of the record value of each record using the Logger  
- Commit records to the cluster whean each record off a poll have been treated  
- See how messages produced with version of the schema are differents from those produced with version 2
#### Update the Avro schema
- replace the WorkshopValue.avsc file with the WorkshopValue.avsc_v2 file  
- compile the project
- consume records from beginning (change your groupid) 
- See how messages produced with version of the schema are differents from those produced with version 2 
 
