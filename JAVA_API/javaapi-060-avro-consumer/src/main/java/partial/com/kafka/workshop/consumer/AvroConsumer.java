package partial.com.kafka.workshop.consumer;

import com.kafka.workshop.avro.WorkshopKey;
import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class AvroConsumer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(AvroConsumer.class);

    // TODO: Set the topic name
    private String topic = TopicsNames.AVRO_SIMPLE_TOPIC;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Topics.createTopics(topic);

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, TopicsNames.PREFIX + "groupid");

        // TODO: Set the Schema Registry url

        // TODO: Set the key and value serializers KafkaAvroSerializer

        // TODO: Set KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG to true


        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

        
            Consumer<WorkshopKey, WorkshopValue> consumer = new KafkaConsumer<>(props);

            // TODO: Subscribe to topic

            try {
                while (true) {

                    // TODO: Poll records and iterate on each records
                    // TODO: Display the record key, the record value, the record partition and the record offset

                    
                        consumer.commitSync();
                    
                }
            } finally {
                consumer.close();
            }
        }

}
