package solution.com.kafka.workshop.consumer;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomAvroDeserializer extends KafkaAvroDeserializer {

    private static final Logger logger = LoggerFactory.getLogger(CustomAvroDeserializer.class);

    @Override
    public Object deserialize(String s, byte[] bytes) {
        try {
            Object o = this.deserialize(bytes);
            return o;
        } catch (Exception e) {
            logger.error("error deserializing message {}", e.getMessage());
            return null;
        }

    }
}
