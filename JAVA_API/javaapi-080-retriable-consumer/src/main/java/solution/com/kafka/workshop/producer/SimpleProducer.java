package solution.com.kafka.workshop.producer;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.Topics;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class SimpleProducer {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        SimpleProducer simpleProducer = new SimpleProducer();
        simpleProducer.start();
    }

    private static final String TOPIC_NAME = TopicsNames.RETRIABLE_TOPIC;
    private final Logger logger = LoggerFactory.getLogger(SimpleProducer.class);
    private final Properties properties = new Properties();
    private final Random random = new Random();


    public SimpleProducer() throws ExecutionException, InterruptedException {
        buildCommonProperties();
    }

    private void start() throws InterruptedException {
        
        Topics.createTopics(TOPIC_NAME);

        logger.info("Sending data to `{}` topic", TOPIC_NAME);
        try (Producer<String, String> producer = new KafkaProducer<>(properties)) {
            while (true) {
                int key = random.nextInt(3);
                ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, "Key " + random.doubles().toString(), "Value " + random.doubles().toString());
                logger.info("Sending Key = {}, Value = {}", record.key(), record.value());
                producer.send(record);
                TimeUnit.SECONDS.sleep(1);
            }
        }
    }

    private void buildCommonProperties() {
        properties.putIfAbsent(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        properties.putIfAbsent(ProducerConfig.ACKS_CONFIG, "all");
        properties.putIfAbsent(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.putIfAbsent(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    }


}
