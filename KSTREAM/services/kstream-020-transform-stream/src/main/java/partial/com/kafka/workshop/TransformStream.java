package partial.com.kafka.workshop;

import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.StreamExecutionContext;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class TransformStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(TransformStream.class);

    private final String topic_in = TopicsNames.TRANSFORM_STREAM_IN;
    private final String topic_out = TopicsNames.TRANSFORM_STREAM_OUT;

    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic_in);
        Topics.createTopics(topic_out);

        try {
            // init serdes
            Map<String, String> serdesConfig = new HashMap<>();
            serdesConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG,
                    KafkaProperties.SCHEMA_REGISTRY);
            StreamExecutionContext.setSerdesConfig(serdesConfig);

            // Create KafkaStreams instance
            Topology topology = getTopology();
            logger.info("topology: {}", topology.describe());
            KafkaStreams streams = new KafkaStreams(getTopology(), configureStream());

            // Start the stream
            streams.start();

            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     *
     * @return
     */
    public Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        // Set the bootstrap servers using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "transform-app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Integer().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    public Topology getTopology() {
        // TODO Create StreamsBuilder instance
        final StreamsBuilder builder = null;

        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, String> stream = null;
        /*
            TODO Re-key the stream using the map method:
                                    set the key, use the extracKey method
                                    set value with a workshopValue object, use the convertWorkshop method
                 Set it to a new KStream object
         */
        KStream<Long, WorkshopValue> transformedStream = null;
        
        /*
            TODO Change the Reference property of workshopValue object, use the setRef method
         */
        
        /*
            TODO Filter the LongDescription property of workshopValue object, accept only ones that start with a digit
                 Use the filterLongDescription method
         */
        
        /*
            TODO Produce the new stream to topic_out by specifying the Serdes for key and value with Produces.with() method
        */


        return builder.build();

    }


    /**
     * Convert a String value object to a WorkshopValue object
     *
     * @param value
     * @return
     */
    public static WorkshopValue convertWorkshop(String value) {
        String[] valueArray = value.split("::");
        Long id = Long.parseLong(valueArray[0]);
        String longDesc = valueArray[1];
        String shortDesc = valueArray[2];
        String reference = valueArray[3];
        // TODO Create a WorkshopValue object
        WorkshopValue workshopValue = null;
        return workshopValue;
    }

    /**
     * check if long descrition starts with a digit
     *
     * @param longDescription
     * @return
     */
    public static boolean filterLongDescription(String longDescription) {
        if (Character.isDigit(StringUtils.remove(longDescription, "long description ").charAt(0))) {
            return true;
        }
        return false;
    }
    /**
     * Extract key as long from String value
     *
     * @param value
     * @return
     */
    public static Integer extractKey(String value) {
        return Integer.parseInt(value.split("::")[0]);
    }

    /**
     * Alter the reference property
     *
     * @param value
     * @return
     */
    public static WorkshopValue setRef(WorkshopValue value) {
        value.setReference("newRef_" + value.getReference());
        return value;
    }
}

