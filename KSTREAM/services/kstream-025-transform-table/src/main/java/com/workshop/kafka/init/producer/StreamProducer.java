package com.workshop.kafka.init.producer;

import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;


public class StreamProducer {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private static String topic_in = TopicsNames.TRANSFORM_STREAM_IN;

    public static void main(String[] args){
        StreamProducer streamProducer = new StreamProducer();
        streamProducer.run();
    }

    public void run() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        Producer<String, String> producer = new KafkaProducer<>(props);

        try {
            
            String value;
            String id;
            for(int i=0; i<100; i++) {
                id = getRandomId();
                value = i + "::" +
                        String.format("long description %s",getAlphaNumericString(10))  + "::" +
                        String.format("short description %s",getAlphaNumericString(3))  + "::" +
                        getAlphaNumericString(5);

                // Send record
                ProducerRecord<String, String> record = new ProducerRecord<>(topic_in, id, value);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    public static String getRandomId() {
        Random rand = new Random();
        List<String> givenList = new ArrayList<>();
        givenList.add("111111");
        givenList.add("222222");
        givenList.add("333333");
        givenList.add("444444");
        givenList.add("555555");
        givenList.add("666666");
        int randomIndex = rand.nextInt(givenList.size());
        String reference = givenList.get(randomIndex);
        return  reference;
    }


}
