package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatAdd;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class StreamStreamProducer {

    private static final Logger logger = LoggerFactory.getLogger(StreamStreamProducer.class);

    private final String topic_stream_left_in = TopicsNames.JOIN_STREAM_LEFT_IN;
    private final String topic_stream_right_in = TopicsNames.JOIN_STREAM_RIGHT_IN;
    
    public static void main(String[] args){
        StreamStreamProducer streamTableProducer = new StreamStreamProducer();
        streamTableProducer.run();
    }
    
    public void run() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopStat> producer = new KafkaProducer<>(props);
        Producer<String, WorkshopStatAdd> producerAdd = new KafkaProducer<>(props);

        try {

            String key;
            WorkshopStat workshopStat;
            int minutes = 0;
            for(int i=0; i<10; i++) {

                key = getRandomId();
                workshopStat = WorkshopStat.newBuilder()
                        .setId(getRandomId())
                        .setDescription(getAlphaNumericString(10))
                        .setStat(getAlphaNumericString(5))
                        .setTimestamp(getTimestamp(minutes + 2*i))
                        .build();

                ProducerRecord<String, WorkshopStat> record = new ProducerRecord<>(topic_stream_left_in, key, workshopStat);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }
            try {

                String key;
                WorkshopStatAdd workshopStatAdd;

                for(int i=0; i<6; i++) {
                    key = getId(i);
                    workshopStatAdd = WorkshopStatAdd.newBuilder()
                            .setId(getRandomId())
                            .setStatAdd(getAlphaNumericString(3))
                            .setTimestamp(getTimestamp(i + 2*i))
                            .build();

                    ProducerRecord<String, WorkshopStatAdd> record = new ProducerRecord<>(topic_stream_right_in, key, workshopStatAdd);
                    producerAdd.send(record, new Callback() {
                        public void onCompletion(RecordMetadata metadata, Exception e) {
                            if (e != null) {
                                logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                            } else {
                                logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                            }
                        }
                    });
                }
         } finally {
                producerAdd.close();
            }
    }

    private static String getId(int index) {
        List<String> givenList = new ArrayList<>();
        givenList.add("111111");
        givenList.add("222222");
        givenList.add("333333");
        givenList.add("444444");
        givenList.add("555555");
        givenList.add("666666");
        String reference = givenList.get(index);
        return  reference;
    }
    
    public static String getRandomId() {
        Random rand = new Random();
        List<String> givenList = new ArrayList<>();
        givenList.add("111111");
        givenList.add("222222");
        givenList.add("333333");
        givenList.add("444444");
        givenList.add("555555");
        givenList.add("666666");
        int randomIndex = rand.nextInt(givenList.size());
        String reference = givenList.get(randomIndex);
        return  reference;
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }
    
    
    private static String getTimestamp(int nano){
        LocalDateTime localDateTime = LocalDateTime.now().minusHours(3).withMinute(0).withSecond(0);
        final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return sdf.format(localDateTime.plusNanos(nano));
    }


}
