package streams;

import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatAdd;
import com.kafka.workshop.avro.WorkshopStatEnriched;
import com.kafka.workshop.utils.SerdesUtils;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import solution.com.kafka.workshop.JoinTimestampExtractor;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StreamStreamWithTimestampExtractorJoinTest extends MockedKafkaClusterTestUtils {


    @Test
    public void joinLeftRightTest() {

        WorkshopStat workshopStat = WorkshopStat.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_left_in.pipeInput("id1", workshopStat);

        WorkshopStatAdd workshopStatAdd = WorkshopStatAdd.newBuilder()
                .setId("id1")
                .setStatAdd("statAdd1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_right_in.pipeInput("id1", workshopStatAdd);

        List<KeyValue<String, WorkshopStatEnriched>> results = stream_stream_out.readKeyValuesToList();

        assertEquals(1, results.size());
        WorkshopStatEnriched workshopStatEnriched = WorkshopStatEnriched.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setStatAdd("statAdd1")
                .setDescription("desc1")
                .build();

        assertEquals(results.get(0).value, workshopStatEnriched);
    }

    @Test
    public void joinRightLeftTest() {

        WorkshopStatAdd workshopStatAdd = WorkshopStatAdd.newBuilder()
                .setId("id1")
                .setStatAdd("statAdd1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_right_in.pipeInput("id1", workshopStatAdd);

        WorkshopStat workshopStat = WorkshopStat.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_left_in.pipeInput("id1", workshopStat);

        List<KeyValue<String, WorkshopStatEnriched>> results = stream_stream_out.readKeyValuesToList();

        assertEquals(1, results.size());
        WorkshopStatEnriched workshopStatEnriched = WorkshopStatEnriched.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setStatAdd("statAdd1")
                .setDescription("desc1")
                .build();

        assertEquals(results.get(0).value, workshopStatEnriched);
    }

    @Test
    public void noJoinLeftRightTest() {

        WorkshopStat workshopStat = WorkshopStat.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_left_in.pipeInput("id1", workshopStat);

        WorkshopStatAdd workshopStatAdd = WorkshopStatAdd.newBuilder()
                .setId("id1")
                .setStatAdd("statAdd1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_right_in.pipeInput("id1", workshopStatAdd, kafkaStreamBuilder.windowDuration + 1);

        List<KeyValue<String, WorkshopStatEnriched>> results = stream_stream_out.readKeyValuesToList();

        assertEquals(1, results.size());
        WorkshopStatEnriched workshopStatEnriched = WorkshopStatEnriched.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setStatAdd("statAdd1")
                .setDescription("desc1")
                .build();

        assertEquals(results.get(0).value, workshopStatEnriched);

    }

    @Test
    public void noJoinRightLeftTest() {

        WorkshopStatAdd workshopStatAdd = WorkshopStatAdd.newBuilder()
                .setId("id1")
                .setStatAdd("statAdd1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_right_in.pipeInput("id1", workshopStatAdd);

        WorkshopStat workshopStat = WorkshopStat.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_left_in.pipeInput("id1", workshopStat, kafkaStreamBuilder.windowDuration + 1);

        List<KeyValue<String, WorkshopStatEnriched>> results = stream_stream_out.readKeyValuesToList();

        assertEquals(1, results.size());
        WorkshopStatEnriched workshopStatEnriched = WorkshopStatEnriched.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setStatAdd("statAdd1")
                .setDescription("desc1")
                .build();

        assertEquals(results.get(0).value, workshopStatEnriched);

    }

    @BeforeEach
    public void testSetup() {

        super.testSetup();
        properties.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, JoinTimestampExtractor.class.getName());

        testDriver = new TopologyTestDriver(kafkaStreamBuilder.getTopologyStreamStreamJoin(), properties);

        stream_left_in = testDriver.createInputTopic(topic_stream_left_in,
                Serdes.String().serializer(), SerdesUtils.<WorkshopStat>getSerdes(false).serializer());
        stream_right_in = testDriver.createInputTopic(topic_stream_right_in, Serdes.String().serializer(),
                SerdesUtils.<WorkshopStatAdd>getSerdes(false).serializer());
        stream_stream_out = testDriver.createOutputTopic(topic_stream_stream_out, Serdes.String().deserializer(),
                SerdesUtils.<WorkshopStatEnriched>getSerdes(false).deserializer());

    }

}


