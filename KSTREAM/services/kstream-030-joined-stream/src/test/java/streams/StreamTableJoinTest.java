package streams;

import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.avro.WorkshopStatEnriched;
import com.kafka.workshop.utils.SerdesUtils;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StreamTableJoinTest extends MockedKafkaClusterTestUtils {

    @Test
    public void joinTest() {        

        WorkshopRef workshopRef = WorkshopRef.newBuilder()
                .setReference("id1")
                .setDescription("descRef")
                .build();
        table_in.pipeInput("id1", workshopRef);

        WorkshopStat workshopStat = WorkshopStat.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setTimestamp(getTimestamp(1))
                .build();

        stream_left_in.pipeInput("id1", workshopStat);
        
        List<KeyValue<String, WorkshopStatEnriched>> results = stream_table_out.readKeyValuesToList();

        assertEquals(1, results.size());
        WorkshopStatEnriched workshopStatEnriched = WorkshopStatEnriched.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setLongDescription("descRef")
                .build();
        
        assertEquals(workshopStatEnriched, results.get(0).value);
    }

    @Test
    public void noJoinTest() {
        
        WorkshopStat workshopStat = WorkshopStat.newBuilder()
                .setId("id1")
                .setStat("stat1")
                .setDescription("desc1")
                .setTimestamp(getTimestamp(1))
                .build();
        stream_left_in.pipeInput("id1", workshopStat);

        WorkshopRef workshopRef = WorkshopRef.newBuilder()
                .setReference("id1")
                .setDescription("descRef")
                .build();
        table_in.pipeInput("id1", workshopRef);

        List<KeyValue<String, WorkshopStatEnriched>> results = stream_table_out.readKeyValuesToList();

        assertEquals(0, results.size());
    }

    @BeforeEach
    public void testSetup() {

        super.testSetup();
        
        testDriver = new TopologyTestDriver(kafkaStreamBuilder.getTopologyStreamTableJoin(), properties);

        stream_left_in = testDriver.createInputTopic(topic_stream_left_in,
                Serdes.String().serializer(), SerdesUtils.<WorkshopStat>getSerdes(false).serializer());
        table_in = testDriver.createInputTopic(topic_table_in, Serdes.String().serializer(), 
                SerdesUtils.<WorkshopRef>getSerdes(false).serializer());
        stream_table_out = testDriver.createOutputTopic(topic_stream_table_out, Serdes.String().deserializer(),
                SerdesUtils.<WorkshopStatEnriched>getSerdes(false).deserializer());
        
    }

}


