package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.ValidWorkshopValue;
import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class StreamProducer {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private final String topic_in = TopicsNames.SPLIT_STREAM_IN;
    private final String topic_valid_in = TopicsNames.SPLIT_VALID_STREAM_IN;

    public static void main(String[] args){
        StreamProducer streamProducer = new StreamProducer();
        streamProducer.run();
    }
    
    public void run() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, ValidWorkshopValue> validWorkshopValueProducer = new KafkaProducer<>(props);
        Producer<String, WorkshopValue> workshopValueProducer = new KafkaProducer<>(props);

        try {

            String key;
            ValidWorkshopValue validWorkshopValue;
            WorkshopValue workshopValue;

            for(int i=0; i<100; i++) {
                
                key = getAlphaNumericString(3);
                validWorkshopValue = ValidWorkshopValue.newBuilder()
                        .setId(getAlphaNumericString(3))
                        .setReference("FROM VALID")
                        .setStat(getAlphaNumericString(3))
                        .setDescription(getAlphaNumericString(50))
                        .build();

                ProducerRecord<String, ValidWorkshopValue> validRecord = new ProducerRecord<>(topic_valid_in, key, validWorkshopValue);
                validWorkshopValueProducer.send(validRecord, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });

                key = getAlphaNumericString(3);
                workshopValue = WorkshopValue.newBuilder()
                        .setId(getAlphaNumericString(3))
                        .setReference("FROM GLOBAL")
                        .setStat(getAlphaNumericString(3))
                        .setDescription(getAlphaNumericString(50))
                        .setValid(isValid())
                        .build();

                ProducerRecord<String, WorkshopValue> record = new ProducerRecord<>(topic_in, key, workshopValue);
                workshopValueProducer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            validWorkshopValueProducer.close();
        }
    }

    private static boolean isValid(){
        if(Math.random() < 0.7){
            return true;
        }
        return false;
    }

 
    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }


}
