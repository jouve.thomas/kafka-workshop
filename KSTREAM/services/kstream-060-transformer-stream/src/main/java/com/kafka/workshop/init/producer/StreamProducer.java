package com.kafka.workshop.init.producer;

import com.kafka.workshop.avro.WorkshopValue;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class StreamProducer {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private final String topic_in = TopicsNames.DEDUPLICATE_TOPIC_IN;

    public static void main(String[] args){
        StreamProducer streamProducer = new StreamProducer();
        streamProducer.run();
    }
    
    public void run() {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopValue> workshopValueProducer = new KafkaProducer<>(props);

        try {

            String key;
            WorkshopValue workshopValue;

            for(int i=0; i<10000; i++) {

                key = getRandomId();
                workshopValue = WorkshopValue.newBuilder()
                        .setId(getAlphaNumericString(3))
                        .setReference(i + "_" + getAlphaNumericString(10))
                        .setDescription(i + "_" + getAlphaNumericString(10))
                        .build();

                ProducerRecord<String, WorkshopValue> record = new ProducerRecord<>(topic_in, key, workshopValue);
                workshopValueProducer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workshopValueProducer.close();
        }
    }

    public static String getRandomId() {
        Random rand = new Random();
        List<String> givenList = new ArrayList<>();
        givenList.add("111111");
        givenList.add("222222");
        givenList.add("333333");
        int randomIndex = rand.nextInt(givenList.size());
        String id = givenList.get(randomIndex);
        return  id;

    }
    
    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }


}
