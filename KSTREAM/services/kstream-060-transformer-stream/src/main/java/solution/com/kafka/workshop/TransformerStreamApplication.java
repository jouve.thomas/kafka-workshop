package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class TransformerStreamApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(TransformerStreamApplication.class);
        application.run(args);
    }
	
}
