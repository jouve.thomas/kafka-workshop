package streams;

import com.kafka.workshop.avro.WorkshopValue;
import org.apache.kafka.streams.KeyValue;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransformerStreamTest extends MockedKafkaClusterTestUtils{

    @Test
    public void deduplicateTest() {
        
        String key = "key-1";
        WorkshopValue workshopValue = WorkshopValue.newBuilder()
                .setId("id-1")
                .setReference("ref-1")
                .setDescription("desc-1")
                .build();

        workshopValueTestInputTopic.pipeInput(key, workshopValue);

        key = "key-1";
        workshopValue = WorkshopValue.newBuilder()
                .setId("id-1")
                .setReference("ref-2")
                .setDescription("desc-2")
                .build();
        workshopValueTestInputTopic.pipeInput(key, workshopValue);

        testDriver.advanceWallClockTime(Duration.ofMillis(60000));

        List<KeyValue<String, WorkshopValue>> validResults = workshopValueTestOutputTopic.readKeyValuesToList();

        assertEquals(1, validResults.size());
        
        WorkshopValue expectedWorkshopValue = WorkshopValue.newBuilder()
                .setId("id-1")
                .setReference("ref-1")
                .setDescription("desc-1")
                .build();
        
        assertEquals(expectedWorkshopValue, validResults.get(0).value);
    }

    @Test
    public void deduplicateMultipleTest() {

        String key = "key-1";
        WorkshopValue workshopValue = WorkshopValue.newBuilder()
                .setId("id-1")
                .setReference("ref-1")
                .setDescription("desc-1")
                .build();

        workshopValueTestInputTopic.pipeInput(key, workshopValue);

        key = "key-1";
        workshopValue = WorkshopValue.newBuilder()
                .setId("id-2")
                .setReference("ref-2")
                .setDescription("desc-2")
                .build();
        workshopValueTestInputTopic.pipeInput(key, workshopValue);

        testDriver.advanceWallClockTime(Duration.ofMillis(60000));

        key = "key-1";
        workshopValue = WorkshopValue.newBuilder()
                .setId("id-3")
                .setReference("ref-3")
                .setDescription("desc-3")
                .build();
        workshopValueTestInputTopic.pipeInput(key, workshopValue);

        testDriver.advanceWallClockTime(Duration.ofMillis(60000));

        List<KeyValue<String, WorkshopValue>> validResults = workshopValueTestOutputTopic.readKeyValuesToList();

        assertEquals(validResults.size(), 2);

        WorkshopValue expectedWorkshopValue_1 = WorkshopValue.newBuilder()
                .setId("id-1")
                .setReference("ref-1")
                .setDescription("desc-1")
                .build();

        WorkshopValue expectedWorkshopValue_2 = WorkshopValue.newBuilder()
                .setId("id-3")
                .setReference("ref-3")
                .setDescription("desc-3")
                .build();

        assertEquals(expectedWorkshopValue_1, validResults.get(0).value);
        
        assertEquals(expectedWorkshopValue_2, validResults.get(1).value);
        
    }

}
