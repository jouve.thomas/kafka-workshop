package solution.com.kafka.workshop.error;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.errors.ProductionExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class CustomKafkaProductionErrorHandler implements ProductionExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(CustomKafkaProductionErrorHandler.class);


    @Override
    public ProductionExceptionHandlerResponse handle(ProducerRecord<byte[], byte[]> record, Exception exception) {
        logger.info("Exception caught during production, " +
                        "key{}, topic: {}, partition: {}, exception: {}",
                record.key(), record.topic(), record.partition(),
                exception);
        return ProductionExceptionHandlerResponse.CONTINUE;
    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
