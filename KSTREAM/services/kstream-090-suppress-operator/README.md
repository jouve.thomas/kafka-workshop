#### This exercise aims at writing a KSTREAM java application merging a KStream and KTable  

The objective of this exercise is to:
- read records from merge-stat-topic topic as a KStream  
- read records from merge-ref-topic topic as a KTable  
- merge the KStream and the KTable into another Stream  
- produce the merged records to merge-intermediate-topic topic  
- apply a filter to merged records and produce filtered records to merge-out-topic topic  

    
#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  

#### Configure the KSTREAM topology and the Join between the KStream and the KTable
Follow the TODO instructions in the partial.com.kafka.workshop.MergeStream and partial.com.kafka.workshop.CustomJoiner java classes   

#### Start the Stream application
Start the application  

#### Produce example records
Use the com.kafka.workshop.init.producer.MergeStreamProducerApplication class to produce records to merge-stat-topic and merge-ref-topic 
The produce records are Avro values representing a WorkshopStat and WorkshopRef objects  

#### Consume example records
Use the com.workshop.kafka.init.consumer.MergeStreamTableConsumerApplication class to consumer records from merge-intermediate-topic and merge-out-topic topics     
Check that merging and filtering are made correctly    
