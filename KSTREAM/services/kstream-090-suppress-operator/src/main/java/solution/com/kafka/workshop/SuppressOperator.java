package solution.com.kafka.workshop;

import com.github.luben.zstd.ZstdOutputStream;
import com.kafka.workshop.avro.WorkshopStat;
import com.kafka.workshop.constants.KafkaProperties;
import com.kafka.workshop.constants.TopicsNames;
import com.kafka.workshop.utils.SerdesUtils;
import com.kafka.workshop.utils.StreamExecutionContext;
import com.kafka.workshop.utils.Topics;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.WindowStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class SuppressOperator implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StateStore.class);

    private String topic_in = TopicsNames.WINDOWS_STREAM_IN;
    private String topic_count = TopicsNames.WINDOWS_STREAM_COUNT_OUT;

    @Override
    public void run(ApplicationArguments args) {

        Topics.createTopics(topic_in);
        Topics.createCompactTopics(topic_count);

        try {
            // init serdes
            Map<String, String> serdesConfig = new HashMap<>();
            serdesConfig.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG,
                    KafkaProperties.SCHEMA_REGISTRY);
            StreamExecutionContext.setSerdesConfig(serdesConfig);
            
              Topology topology = getTopologyCount();
            logger.info("topology: {}", topology.describe());
            KafkaStreams streams = new KafkaStreams(topology, configureStream());
            streams.start();
            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    public Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaProperties.BOOTSTRAP_SERVERS);
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY);
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, TopicsNames.PREFIX + "window-app-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // TODO set the DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG property
        streamsConfiguration.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WorkshopStatTimestampExtractor.class.getName());
        // Set the replication factor for internal topics to 3
        streamsConfiguration.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 3);
        // Set the min in sync replicas for internal topics to 2
        streamsConfiguration.put(StreamsConfig.topicPrefix(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG), 2);
        // Set default tmp folder
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, KafkaProperties.STORE_DIR);
        //     streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        return streamsConfiguration;
    }

    /**
     * @return
     */
    public Topology getTopologyCount() {
        final StreamsBuilder builder = new StreamsBuilder();

         /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_stream_in by specifying the Serdes for key and value with Consumed.with() method
                 Re-key  the stream using the map method: set the key with the ident property
                 Group the stream by key
                 Using the windowedBy method, set the duration to 10 minutes
                 Transform to stream to a KTable<Windowed<String>, Long> unsing the count() method
                 Transform the KTable to KStream
                 Re-key the stream using the map method:
                                    set the key by extracting the key of the Windowed<String> object
                                        (Windowed<String> key, Long count)  -> new KeyValue(...)
                                    apply the toString() method to the count value
                 Produce the stream to window_count

        */
       // KStream<String, String> windowStream = 
                builder.stream(topic_in, Consumed.with(Serdes.String(), JsonSerdes.Workshop()))
                .groupByKey()
                .windowedBy(
                        TimeWindows.of(Duration.ofMinutes(2))
                                .grace(Duration.ofSeconds(2))
                        )
                                .count(Materialized.as("count-metric"))
                                .suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                              //  .filter( (k,v) -> v < 4 )
                                .toStream().foreach(
                                        (k,v) -> System.out.println("yoy = " + k.key() + " == "  + k.window().start()
                                                + " == "  + k.window().end()
                                                + " == " + v)
                                );
            //    .to(topic_count, Produced.with(Serdes.String(), Serdes.String()));

        return builder.build();
    }

    
    

}

