package solution.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class SuppressOperatorApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SuppressOperatorApplication.class);
        application.run(args);
    }
	
}
