package solution.com.kafka.workshop;

public class Workshop {

    private String timestamp;
    
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
              
}
