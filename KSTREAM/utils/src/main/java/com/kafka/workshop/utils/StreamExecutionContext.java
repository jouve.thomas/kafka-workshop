package com.kafka.workshop.utils;

import java.util.Map;

public class StreamExecutionContext {

    private static Map<String, String> serdesConfig;
    
    private static int partitionNumber;

    public StreamExecutionContext() {
    }

    public static Map<String, String> getSerdesConfig() {
        return serdesConfig;
    }

    public static void setSerdesConfig(Map<String, String> serdesConfig) {
        StreamExecutionContext.serdesConfig = serdesConfig;
    }

    public static int getPartitionNumber() {
        return partitionNumber;
    }

    public static void setPartitionNumber(int partitionNumber) {
        StreamExecutionContext.partitionNumber = partitionNumber;
    }
}
