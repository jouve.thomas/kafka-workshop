# USAGES

This project is intented to play with Kafka  and and its ecosystem  
Each folder contains a docker-compose file to start a Kafka Cluster  
Follow the README.md file instructions    

##### - CONSOLE folder
Play with Kafka command line tools  


##### - REGISTRY folder
Play with Kafka Schema Registry endpoints  


##### - JAVA_API folder
Play with Kafka JAva Api to produce and consumer from Kafka Cluster  


##### - KSTREAM folder  
Play with Kafka Stream JAva Api to transform data from Kafka to Kafka
