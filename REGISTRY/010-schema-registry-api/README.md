#### This exercise aims at interacting with the schema registry API

#### Start the Kafka cluster :
In the CONSOLE directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  
The cluster is composed of 1 zookeeper, 3 brokers and 1 schema-registry 
The docker-compose file contains also a base container that allows to run Kafka commands

#### Schema registry API
Using a API testing tool (Postman),
- Get the global compatibility  
```
GET http://localhost:8081/config/
```    
- update the global compatibility to FORWARD_TRANSITIVE  
```
PUT http://localhost:8081/config/
{
  "compatibility": "FORWARD_TRANSITIVE"
} 	
```
- post a schema named SCHEMA-TEST
        {
            "type":"record",
            "name":"schemaTest",
            "namespace":"com.kafka.workshop.avro",
            "doc":"this is a schema test",
            "fields":
                [
                    {"name":"keyField1","type":"string","default":"keyField1"},
                    {"name":"keyField2","type":"string","default":"keyField2"}
                ]
        }
```
POST  http://localhost:8081/subjects/SCHEMA-TEST/versions  
{
    "schema": "{\n            \"type\":\"record\",\n            \"name\":\"schemaTest\",\n            \"namespace\":\"com.kafka.workshop.avro\",\n            \"doc\":\"this is a schema test\",\n            \"fields\":\n                [\n                    {\"name\":\"keyField1\",\"type\":\"string\",\"default\":\"keyField1\"},\n                    {\"name\":\"keyField2\",\"type\":\"string\",\"default\":\"keyField2\"}\n                ]\n        }"
} 
```
- get the detail of the version 1 of SCHEMA-TEST
```
GET http://localhost:8081/subjects/SCHEMA-TEST/versions/1
 ```
- get a pretty detail of the version 1 of SCHEMA-TEST
```
GET http://localhost:8081/subjects/SCHEMA-TEST/versions/1/schema
```
- post a modification of SCHEMA-TEST
  {
    "type":"record",
    "name":"schemaTest",
    "namespace":"com.kafka.workshop.avro",
    "doc":"this is a schema test",
    "fields":
        [
            {"name":"keyField1","type":"string","default":"keyField1"},
            {"name":"keyField2","type":"string","default":"keyField2"},
            {"name":"keyField3","type":"string"}
        ]
  }
```
POST  http://localhost:8081/subjects/SCHEMA-TEST/versions  
{
    "schema": "{\"type\":\"record\",\"name\":\"schemaTest\",\"namespace\":\"com.kafka.workshop.avro\",\"doc\":\"this is a schema test\",\"fields\":[{\"name\":\"keyField1\",\"type\":\"string\",\"default\":\"keyField1\"},{\"name\":\"keyField2\",\"type\":\"string\",\"default\":\"keyField2\"},{\"name\":\"keyField3\",\"type\":\"string\"}]}"
} 
```
- get the list of schemas
```
GET http://localhost:8081/subjects/
```
- get the list of versions of SCHEMA-TEST
```
GET http://localhost:8081/subjects/SCHEMA-TEST/versions
```
- get the detail of the version 2 of SCHEMA-TEST
```
GET http://localhost:8081/subjects/SCHEMA-TEST/versions/2
 ```
- get a pretty detail of the version 2 of SCHEMA-TEST
```
GET http://localhost:8081/subjects/SCHEMA-TEST/versions/2/schema
```
- check the compatibility with a new schema
  {
    "type":"record",
    "name":"schemaTest",
    "namespace":"com.kafka.workshop.avro",
    "doc":"this is a schema test",
    "fields":
        [
            {"name":"keyField1","type":"string","default":"keyField1"}
        ]
  }
```
POST  http://localhost:8081/compatibility/subjects/SCHEMA-TEST/versions/1  
{
    "schema": "{\"type\":\"record\",\"name\":\"schemaTest\",\"namespace\":\"com.kafka.workshop.avro\",\"doc\":\"this is a schema test\",\"fields\":[{\"name\":\"keyField1\",\"type\":\"string\",\"default\":\"keyField1\"}]}"
} 
```
- remove the two versions of SCHEMA-TEST
```
DELETE  http://localhost:8081/subjects/SCHEMA-TEST/versions/1  
DELETE  http://localhost:8081/subjects/SCHEMA-TEST/versions/2
```