package com.kafka.workshop.utils;

import com.kafka.workshop.constants.KafkaProperties;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.TopicDescription;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Topics {
    
    public static void createTopics(String topic) {

        createTopics(topic, 3);
  
    }

    public static void createTopics(String topic, int partitions) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", KafkaProperties.BOOTSTRAP_SERVERS);
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                partitions,
                (short) 3));

        client.createTopics(topics);

        client.close();
    }

    public static void createCompactTopics(String topic) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", KafkaProperties.BOOTSTRAP_SERVERS);
      
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();
        Map<String, String> additionalProperties = new HashMap<>();
        additionalProperties.put("cleanup.policy", "compact");
        NewTopic newTopic = new NewTopic(topic, 3, (short) 3);
        newTopic.configs(additionalProperties);
        topics.add(newTopic);

        client.createTopics(topics);
        client.close();
    }
    
    
}
